# debian_fresh_install

orig ref: <https://wiki.strat.zone/en/debian/fresh_install> to use it:

```
apt install curl
bash <(curl --silent https://gitlab.com/strategiczone/debian_fresh_install/raw/master/debian_fresh_install.sh)
```

## Package list to install
Just edit this file `packages.txt`


## Add SSH key using argument:
Example to add Valeriu's key
```
bash <(curl --silent https://gitlab.com/strategiczone/debian_fresh_install/raw/master/debian_fresh_install.sh) -k val
```
Aviable ssh users:
 - val
 - mag
 - raph

## SSH keys
Magomed<br>
`sh <(curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/install_pubkey_magomed.sh)`

Valeriu<br>
`sh <(curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/install_pubkey_valerius.sh)`

Raphael<br>
`sh <(curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/install_pubkey_raphael.sh)`
