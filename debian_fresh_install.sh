#!/usr/bin/env bash
: <<COMMENTBLOCK
title			:Debian fresh install automatization
description	:
author			:Magomed Gamadaev
email			:mag@strategic.zone
date			:31082019
version			:0.3
notes			:
================================================
COMMENTBLOCK


# Banner
echo "IHwgICAgICAgICAgICAgX19ffCAgfCAgICAgICAgICAgICB8ICAgICAgICAgICAgIF8pICAgICBf
XyAgLyAgICAgICAgICAgICAgICAgIAogX18gXCAgfCAgIHwgXF9fXyBcICBfX3wgIF9ffCBfYCB8
IF9ffCAgXyBcICBfYCB8IHwgIF9ffCAgIC8gICBfIFwgIF9fIFwgICBfIFwgCiB8ICAgfCB8ICAg
fCAgICAgICB8IHwgICB8ICAgKCAgIHwgfCAgICBfXy8gKCAgIHwgfCAoICAgICAvICAgKCAgIHwg
fCAgIHwgIF9fLyAKXy5fXy8gXF9fLCB8IF9fX19fLyBcX198X3wgIFxfXyxffFxfX3xcX19ffFxf
XywgfF98XF9fX3xfX19ffFxfX18vIF98ICBffFxfX198IAogICAgICBfX19fLyAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgfF9fXy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
Cg==" | base64 -d


# ==================Variables===================
# Colors
def_color="\e[39m"
grn_color="\e[93m"
red_color="\e[91m"
yel_color="\e[93m"

TZ="Europe/Paris"
# ==============================================

echo -e "\n\n${yel_color}Debian fresh installation script${def_color}\n\n\n"
sleep 3

# Configure timezone and locale
echo "${TZ}" > /etc/timezone
timedatectl set-timezone ${TZ}
dpkg-reconfigure -f noninteractive tzdata
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
sed -i -e 's/# en_US ISO-8859-1/en_US ISO-8859-1/' /etc/locale.gen
sed -i -e 's/# en_US.ISO-8859-15\ ISO-8859-15/en_US.ISO-8859-15\ ISO-8859-15/' /etc/locale.gen

sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen
sed -i -e 's/# fr_FR ISO-8859-1/fr_FR ISO-8859-1/' /etc/locale.gen
sed -i -e 's/# fr_FR@euro\ ISO-8859-15/fr_FR@euro\ ISO-8859-15/' /etc/locale.gen

echo 'LANG="fr_FR.UTF-8"'>/etc/default/locale
dpkg-reconfigure --frontend=noninteractive locales
update-locale LANG=fr_FR.UTF-8

#Enable NTP
timedatectl set-ntp true

# Set hostname
echo -e "\n${yel_color}Configuring hostname, please enter it:\n${def_color}"
read hostname
hostnamectl set-hostname ${hostname}
sed -i "/127.0.1.1*/c\127.0.1.1\t${hostname}" "/etc/hosts"

# Configure ssh server
sed -i "/\PermitRootLogin\ yes/c\PermitRootLogin prohibit-password" /etc/ssh/sshd_config
sed -i "/\Port\ 22/c\Port\ 34522" /etc/ssh/sshd_config


# Debian repository
echo "
#------------------------------------------------------------------------------#
#                   OFFICIAL DEBIAN REPOS by SZ
#------------------------------------------------------------------------------#

###### Debian Main Repos
deb http://deb.debian.org/debian/ buster main contrib non-free
deb-src http://deb.debian.org/debian/ buster main contrib non-free

deb http://deb.debian.org/debian/ buster-updates main contrib non-free
deb-src http://deb.debian.org/debian/ buster-updates main contrib non-free

deb http://deb.debian.org/debian-security buster/updates main
deb-src http://deb.debian.org/debian-security buster/updates main" | tee /etc/apt/sources.list

# Upgrade
apt-get update && apt-get -y upgrade

# Install packages from Gitlab packages list
apt -y install $(curl -s https://gitlab.com/strategiczone/debian_fresh_install/raw/master/packages.txt)

# Install Tools (Oh My ZSH & Transfer SH)
source <(curl --silent https://gitlab.com/strategic.zone/install_ohmyzsh/raw/master/install_ohmyzsh.sh)
source <(curl --silent https://gitlab.com/strategic.zone/transfersh-alias/raw/master/transfersh_install.sh)

## Installation de motd
echo "#!/bin/sh
echo $(hostname) | awk '{print toupper($0)}' | figlet -tc
figlet -tc -f shadow 'by strategic zone'
echo ''
/usr/bin/screenfetch -E
echo
echo 'SYSTEM DISK USAGE'
inxi -D
echo
# Show weather information. Change the city name to fit your location
#ansiweather -l Paris -f 2 -a true -d true
curl 'fr.wttr.in/Paris?format=%l:+%m+%t+%c+%w+%h'
echo " | tee /etc/update-motd.d/01-szmsg
chmod +x /etc/update-motd.d/01-szmsg

# Tips
sed -i 's|termcapinfo vt100 dl=5\\E\[M|termcapinfo xterm* ti@:te@|g' /etc/screenrc

# SSH keys installation
#sh <(curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/install_pubkey_valerius.sh)

# =====================SSH======================
usage() { echo -e "${red_color}Usage: $0 [-k username]${def_color}" 1>&2; exit 1; }

ssh_users=("val" "mag" "raph")
ssh_key_val=$(curl -s https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_ecdsa_valerius-sz.pub)
ssh_key_mag=$(curl -s https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_ecdsa_mg-laptop.pub)
ssh_key_raph=$(curl -s https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_rsa_raphael_lalung.pub)

while getopts ":k:" OPTION; do
    case "${OPTION}" in
        k)
            required_user="${OPTARG}"
            for user in "${ssh_users[@]}"
            do
                if [ "${user}" == "${required_user}" ] ; then
                    mkdir -p ~/.ssh
                    download="ssh_key_${required_user}"
                    grep -q "${!download}" ~/.ssh/authorized_keys || echo "${!download}" >> ~/.ssh/authorized_keys
#                    echo -e "${yel_color}\n${required_user}'s ssh key added${def_color}"
                    chmod 700 ~/.ssh
                    chmod 600 ~/.ssh/authorized_keys
                fi
            done
            ;;
        *)
            echo -e "${red_color}Aviable users:"
            for user in "${ssh_users[@]}"; do
                echo "${user}"
            done
            echo -e "${def_color}"
            usage
            ;;
    esac
done
shift $((OPTIND-1))
# ==============================================

# **** neet to recheck ****
#echo -e "\n${yel_color}SSH keys installation, please enter username, valid users: (${users})${def_color}"
#while read key_name; do
#	if [[ ${key_name} == "magomed" ]] || [[ ${key_name} == "valeriu" ]]; then
#		download="ssh_key_${key_name}"
#		eval ${!download}
#		echo -e "\n${grn_color} ${key_name}'s key added ${def_color} \n(type next user or q to skip)"
#	elif [[ ${key_name} == "" ]]; then
#		echo -e "\n${red_color}Valid users: ${users} \n${def_color}(q to skip)"
#	elif [[ ${key_name} == "q" ]]; then
#		break
#	else
#		echo -e "\n${red_color} ${key_name} not a valid name, enter users one per one. Valid users: ${users} \n${def_color}(q to skip)"
#	fi
#done

# Firewall
echo -e "${red_color}for the firewall check: https://wiki.strat.zone/en/debian/install_nftables${def_color}"
# Reboot

echo -e "${yel_color}Done, have a nice day!${def_color}"
